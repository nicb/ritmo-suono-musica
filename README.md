# *RITMO, SUONO, MUSICA: LA NOZIONE MUSICALE DI SIMMETRIA*

![CC BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

(currently this lecture is exclusively in italian)

This lecture tries to outline two different kinds of simmetries which appear
in music at different levels of perception. The first kind is related to
microscopic time domains (microseconds and milliseconds) and the second is
related to the macroscopic time domains (seconds, minutes, hours).
In increasing degree of abstraction, these two domains mark where the music
begins and which heights it can get to (in perceptual terms).

# DELIVERED

1. DIMED - Convegno *LA MUSICA INCONTRA LA MEDICINA RITMO, SUONI E SALUTE*, Padova, Liviano, Sala dei Giganti, November 23-25 2022

# LICENSE

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img
alt="Creative Commons License" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This
work is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons
Attribution-ShareAlike 4.0 International License</a>.

Please read the [License](./LICENSE.md) for more information.
