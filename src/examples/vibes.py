import pdb
import sys, os
import numpy as np
import scipy.io.wavfile as wavf
import matplotlib.pyplot as plt

imagepath = os.path.join('..', 'images')
sr = 48000
sinc = 1.0/sr
freq = 220.0
per  = 1/freq
start = -0.5
dur  = 5.0
phase = -(np.pi/7.0)
t = np.linspace(start, start+dur, num=np.int(np.floor((start+dur)*sr)))
single_wave = np.cos(2*np.pi*freq*t+phase)

plt.plot(t, single_wave)
plt.axis([-1.5*per, 2*per, -1, 1])
plt.savefig(os.path.join(imagepath, 'single_wave.png'), dpi=300, bbox_inches='tight', pad_inches=0)
wavf.write("./single_wave.wav", sr, single_wave*0.5)

beat_freq1 = freq + 10
beat_wave1  = np.cos(2*np.pi*beat_freq1*t+phase)

plt.cla()
plt.plot(t, single_wave, t, beat_wave1)
plt.axis([-1.5*per, 2*per, -1, 1])
plt.savefig(os.path.join(imagepath, 'beat_and_single.png'), dpi=300, bbox_inches='tight', pad_inches=0)

beat_freq2 = freq + 4.1
beat_wave2  = np.cos(2*np.pi*beat_freq2*t+phase)
result_wave = 0.5*(single_wave+beat_wave2)
plt.cla()
plt.plot(t, result_wave)
plt.axis([start/15.0, (start+dur)/15.0, -1.01, 1.01])
plt.savefig(os.path.join(imagepath, 'beat_and_single_full.png'), dpi=300, bbox_inches='tight', pad_inches=0)
wavf.write("./result_wave.wav", sr, result_wave*0.5)

beat_freq3 = freq + 0.77
beat_wave3  = np.cos(2*np.pi*beat_freq3*t+phase)
beat_freq4 = freq + 0.1
beat_wave4  = np.cos(2*np.pi*beat_freq4*t+phase)
complex_wave = 0.25*(single_wave+beat_wave2+beat_wave3+beat_wave4)
plt.cla()
plt.plot(t, complex_wave)
plt.axis([start/2.0, (start+dur)/2.0, -1.01, 1.01])
plt.savefig(os.path.join(imagepath, 'complex_wave.png'), dpi=300, bbox_inches='tight', pad_inches=0)
wavf.write("./complex_wave.wav", sr, complex_wave*0.5)
